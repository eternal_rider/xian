var resourceArray = [
	{name: "sapling_room",  type:"image", src: "data/img/map/sapling_room.jpg"},
    {name: "teahouse-min",  type:"image", src: "data/img/map/teahouse-min.jpg"},
    {name: "teahouse-min",  type:"image", src: "data/img/map/teahouse-min.jpg"},
	{name: "luocunzhangjia-min",  type:"image", src: "data/img/map/luocunzhangjia-min.jpg"},
    {name: "smithy-min",  type:"image", src: "data/img/map/smithy-min.jpg"},
    {name: "dialog",        type:"image",	src: "data/img/gui/dialog.png"},
	{name: "coingold",        type:"image",	src: "data/img/gui/coin_gold.png"},
    {name: "itemflask",        type:"image",	src: "data/img/gui/flask.png"},
	{name: "quest_point",        type:"image",	src: "data/img/gui/quest_point.png"},
	
        /* earth */
        {name: "barrel", type:"image",	src: "data/img/barrel.png"},
        {name: "bridges", type:"image",	src: "data/img/bridges.png"},
        {name: "buckets", type:"image",	src: "data/img/buckets.png"},
        {name: "dirt", type:"image",	src: "data/img/dirt.png"},
        {name: "dirt2", type:"image",	src: "data/img/dirt2.png"},
       {name:  "doors", type:"image",	src: "data/img/doors.png"},
        {name: "farming_fishing", type:"image",	src: "data/img/farming_fishing.png"},
        {name: "fence", type:"image",	src: "data/img/fence.png"},
        {name: "fence_alt", type:"image",	src: "data/img/fence_alt.png"},
       {name:  "flowers_2", type:"image",	src: "data/img/flowers_2.png"},
       {name:  "grassalt_flowers", type:"image",	src: "data/img/grassalt_flowers.png"},
        {name: "housey", type:"image",	src: "data/img/housey.png"},
       {name:  "misc", type:"image",	src: "data/img/misc.png"},
        {name: "mountains", type:"image",	src: "data/img/mountains.png"},
        {name: "shadow", type:"image",	src: "data/img/shadow.png"},
       {name:  "signs", type:"image",	src: "data/img/signs.png"},
        {name: "stonepattern", type:"image",	src: "data/img/stonepattern.png"},
        {name: "town_buildings", type:"image",	src: "data/img/town_buildings.png"},
       {name:  "tree_stump", type:"image",	src: "data/img/tree_stump.png"},
       {name:  "victorian_house", type:"image",	src: "data/img/victorian_house.png"},
       {name:  "windmill", type:"image",	src: "data/img/windmill.png"},
        {name:  "animwater", type:"image",	src: "data/img/animwater.png"},
        {name:  "grass", type:"image",	src: "data/img/grass.png"},
        {name:  "sandwater", type:"image",	src: "data/img/sandwater.png"},
        {name:  "treetop", type:"image",	src: "data/img/treetop.png"},
       {name:   "trunk", type:"image",	src: "data/img/trunk.png"},
	   {name:   "house",type:"image",	src: "data/img/house.png"},

    /*
     * sprite.
     */
	{name: "player",       type:"image",	src: "data/img/sprite/player.png"},
	{name: "ghost",       type:"image",	src: "data/img/sprite/ghost.png"},
    {name: "sleep",       type:"image",	src: "data/img/sprite/sleep.png"},
    {name: "flare",       type:"image",	src: "data/img/sprite/flare.png"},
    {name: "treeelves",       type:"image",	src: "data/img/sprite/treeelves.png"},
	{name: "werewolf",       type:"image",	src: "data/img/sprite/werewolf.png"},
    {name: "elf",       type:"image",	src: "data/img/sprite/elf.png"},
	

	/* 
	 * Maps. 
 	 */
	{name: "xiaoshu_fangjian",              type: "tmx",	src: "data/map/xiaoshu_fangjian.tmx"},
    {name: "teahouse",              type: "tmx",	src: "data/map/teahouse.tmx"},
    {name: "luocunzhangjia",              type: "tmx",	src: "data/map/luocunzhangjia.tmx"},
    {name: "xiaozhen_shang",              type: "tmx",	src: "data/map/xiaozhen_shang.tmx"},
    {name: "smithy",              type: "tmx",	src: "data/map/smithy.tmx"},

    /*
     * json.
     */
    {name: "player",              type: "json",	src: "data/img/sprite/player.json"},
	{name: "ghost",              type: "json",	src: "data/img/sprite/ghost.json"},
    {name: "flare",       type:"json",	src: "data/img/sprite/flare.json"},
    {name: "treeelves",       type:"json",	src: "data/img/sprite/treeelves.json"},
	{name: "werewolf",       type:"json",	src: "data/img/sprite/werewolf.json"},
    {name: "elf",       type:"json",	src: "data/img/sprite/elf.json"},

    {name: "bg1", type: "audio", src: "data/bgm/"},
    {name: "bg2", type: "audio", src: "data/bgm/"},
    {name: "bg3", type: "audio", src: "data/bgm/"},
    {name: "bg4", type: "audio", src: "data/bgm/"},
    {name: "bg5", type: "audio", src: "data/bgm/"},
    {name: "bg6", type: "audio", src: "data/bgm/"},
    {name: "bg7", type: "audio", src: "data/bgm/"},
    {name: "bg8", type: "audio", src: "data/bgm/"},
    {name: "bg9", type: "audio", src: "data/bgm/"},
    {name: "bg10", type: "audio", src: "data/bgm/"},
    {name: "bg11", type: "audio", src: "data/bgm/"},
    {name: "bg12", type: "audio", src: "data/bgm/"},

	/* 
	 * Sound effects. 
	 */
	{name: "banyuezhan", type: "audio", src: "data/sfx/"},
	{name: "jifengci",  type: "audio", src: "data/sfx/"},
    {name: "huodanshu1",  type: "audio", src: "data/sfx/"},
	{name: "huodanshu2",  type: "audio", src: "data/sfx/"},
	{name: "baiguling",  type: "audio", src: "data/sfx/"},
	{name: "laichangshixiayigejieduan",  type: "audio", src: "data/sfx/"},
    {name: "hurt",  type: "audio", src: "data/sfx/"},
	{name: "dead_WereWolf",  type: "audio", src: "data/sfx/"},
	{name: "dead_Elf",  type: "audio", src: "data/sfx/"},
	{name: "dead_TreeElves",  type: "audio", src: "data/sfx/"},
	
	{name: "cling",  type: "audio", src: "data/sfx/"},
    {name: "heyaoshui",  type: "audio", src: "data/sfx/"},
	{name: "swordbuild",  type: "audio", src: "data/sfx/"},
	{name: "yaonvlile",  type: "audio", src: "data/sfx/"},
	{name: "qianbugou",  type: "audio", src: "data/sfx/"},
    {name: "zsgbjh",  type: "audio", src: "data/sfx/"},
    {name: "jhxywlzj",  type: "audio", src: "data/sfx/"}
];
